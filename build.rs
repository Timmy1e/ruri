use std::{
    env::var_os,
    fs,
    io::{ErrorKind, Result},
    path::PathBuf,
};

use clap::CommandFactory;
use clap_mangen::Man;

use crate::config::Config;

#[path = "src/model/config.rs"]
mod config;

fn main() -> Result<()> {
    let out_dir = PathBuf::from(var_os("OUT_DIR").ok_or(ErrorKind::NotFound)?);

    let mut buffer = Vec::new();
    Man::new(Config::command()).render(&mut buffer)?;

    fs::write(out_dir.join("ruri.1"), buffer)
}
