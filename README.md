# Ruri
[![Pipeline status](https://img.shields.io/gitlab/pipeline-status/Timmy1e/ruri?branch=master&label=Pipeline&logo=gitlab)](https://gitlab.com/Timmy1e/ruri/-/pipelines/)
[![Crates version](https://img.shields.io/crates/v/ruri?label=Crates.io&logo=rust)](https://crates.io/crates/ruri)
[![AUR version](https://img.shields.io/aur/version/ruri?label=ruri&logo=archlinux)](https://aur.archlinux.org/packages/ruri/)
[![AUR version](https://img.shields.io/aur/version/ruri-bin?label=ruri-bin&logo=archlinux)](https://aur.archlinux.org/packages/ruri-bin/)
[![Homebrew tap](https://img.shields.io/badge/Homebrew-tap-orange?logo=Homebrew)](https://github.com/Timmy1e/homebrew-tap/)

Calculates the CRC-32 of files and checks them against their filename.

## Installation
### From repo
Ruri is available in the following repos:
 - Arch Linux AUR: [ruri](https://aur.archlinux.org/packages/ruri/), [ruri-bin](https://aur.archlinux.org/packages/ruri-bin/)
 - Crates.io: [ruri](https://crates.io/crates/ruri)
 - Homebrew: [Timmy1e/tap/ruri](https://github.com/Timmy1e/homebrew-tap/blob/main/Formula/ruri.rb)

### From release
 1. Download a pre-built binary archive from the [releases page](https://gitlab.com/Timmy1e/ruri/-/releases).
 2. Extract the archive.
 3. Move the executable to a logical location such as your `/usr/bin/` or `/usr/local/bin/` directory.

### From source
#### Requirements
- Rust >= 1.64

 1. Pull the project, or download an archive of the source from GitLab and extract it.
 2. Install a [Rust toolchain](https://www.rust-lang.org/tools/install).
 3. Run the following in the root directory to build an executable to `./bin/`:
    ```shell
    $ cargo install --locked --bin ruri --path . --root .
    ```
 4. Move or link the exe executable to a logical location such as your `/usr/bin/` or `/usr/local/bin/` directory.

## Usage
```
Usage: ruri [OPTIONS] [FILE_PATHS]...

Arguments:
  [FILE_PATHS]...  Paths to files you want to check

Options:
  -t, --threads <THREADS>  Override the amount of threads, 0 is CPU max [default: 0]
  -q, --quiet              Don't print progress bars, for piping to files
  -h, --help               Print help
  -V, --version            Print version
```

Basic examples:
```shell
$ ruri "myFile[ABCD1234].ext" myFiles*.ext
```
```shell
$ ruri -q myFiles*.ext > myFiles.crc32
```

## Licence
GNU Affero General Public License v3.0 or later
