/*
 * Copyright © 2022  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    collections::BTreeMap,
    path::PathBuf,
    sync::{Arc, Mutex},
};

use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use lazy_static::lazy_static;
use threadpool::ThreadPool;

use crate::{
    check_file::check_file,
    model::{Config, FileCheckResult},
};

const PROGRESS_STYLE_TEMPLATE: &str =
    "{msg:<40!} {wide_bar:.cyan/blue} {binary_bytes_per_sec:>} {eta_precise:>}";
lazy_static! {
    static ref PROGRESS_STYLE: ProgressStyle = ProgressStyle::default_bar()
        .template(PROGRESS_STYLE_TEMPLATE)
        .expect("Progress bar style to be correct");
}

/// Check files in a config asynchronously
pub fn check_files(config: &Config) -> BTreeMap<PathBuf, FileCheckResult> {
    // Create a simple copy of "is_quiet" for some tiny performance improvements
    let is_quiet = config.is_quiet();
    // Create a thread pool with the configured amount of threads
    let pool = ThreadPool::new(config.threads());
    // Create an AtomizedReferenceCounting Mutex Vector of FileCheckResults
    let results = Arc::new(Mutex::new(BTreeMap::new()));
    // Create the multi-progress
    let multi_progress = MultiProgress::new();

    // Loop through the file paths
    for file_path in config.file_paths() {
        // Create a progress bar if we aren't quiet
        let progress_bar = if !is_quiet {
            let progress_bar = multi_progress.add(ProgressBar::new(0));
            progress_bar.set_style(PROGRESS_STYLE.clone());

            Some(progress_bar)
        } else {
            None
        };

        // Clone vars, to move into the lambda expression
        let file_path = file_path.clone();
        let results = results.clone();
        // Start another thread using the pool
        pool.execute(move || {
            // Check the file, and possibly get the CRC32 sum
            let result = check_file(&file_path, progress_bar);
            // Get a lock on results, and push this result
            if let Ok(mut results) = results.lock() {
                results.insert(file_path, result);
            }
        });
    }
    // Display the progress bars
    multi_progress
        .clear()
        .expect("Progress bars to be complete");
    // Join the threads
    pool.join();

    // Attempt to collect the results
    let results = Arc::try_unwrap(results).expect("Arc to be unused");
    results.into_inner().expect("Mutex to be unlocked")
}
