/*
 * Copyright © 2021  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::path::PathBuf;

use clap::Parser;

/// A Ruri config object, should be passed to check_files
#[derive(Debug, Parser)]
#[command(name = "Ruri")]
#[command(author = clap::crate_authors!("\n"))]
#[command(version = clap::crate_version!())]
#[command(about = clap::crate_description!())]
#[command(bin_name = clap::crate_name!())]
pub struct Config {
    /// Paths to files you want to check
    file_paths: Vec<PathBuf>,
    #[arg(short, long, default_value_t = 0)]
    /// Override the amount of threads, 0 is CPU max
    threads: usize,
    #[arg(short, long)]
    /// Don't print progress bars, for piping to files
    quiet: bool,
}

impl Config {
    #[allow(dead_code)]
    /// Borrow the file paths as a String Vec
    pub fn file_paths(&self) -> &Vec<PathBuf> {
        &self.file_paths
    }

    #[allow(dead_code)]
    /// Get the requested thread count
    pub fn threads(&self) -> usize {
        if self.threads < 1 {
            num_cpus::get()
        } else {
            self.threads
        }
    }

    #[allow(dead_code)]
    /// Is set as quiet
    pub fn is_quiet(&self) -> bool {
        self.quiet
    }
}
