/*
 * Copyright © 2021  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::fmt::{Display, Formatter, Result};

use colored::Colorize;

use crate::model::Status;

/// Non-error result from file check
#[derive(Debug)]
pub struct FileCheckAnswer {
    pub file_name: String,
    pub crc_found: String,
    pub crc_calculated: String,
    pub status: Status,
}

impl Display for FileCheckAnswer {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match self.status {
            Status::Missing => write!(
                f,
                "{}\t{}",
                format!("⁉  {}", self.crc_calculated).yellow(),
                self.file_name
            ),
            Status::Ok | Status::Wrong => {
                let mut file_name_split = self.file_name.split(&self.crc_found);
                let (crc_calculated, crc_found) = if self.status == Status::Ok {
                    (
                        format!("✓  {}", self.crc_calculated).green(),
                        self.crc_found.green(),
                    )
                } else {
                    (
                        format!("×  {}", self.crc_calculated).red(),
                        self.crc_found.red(),
                    )
                };
                write!(
                    f,
                    "{}\t{}{}{}",
                    crc_calculated,
                    file_name_split.next().unwrap(),
                    crc_found,
                    file_name_split.next().unwrap()
                )
            }
        }
    }
}
