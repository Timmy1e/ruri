/*
 * Copyright © 2022  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    collections::BTreeMap,
    time::{Duration, Instant},
};

use colored::Colorize;
use humantime::format_duration;

use crate::model::{FileCheckResult, Status};

/// A struct to keep track of the results
#[derive(Debug)]
pub struct ResultTracker {
    ok: usize,
    error: usize,
    wrong: usize,
    missing: usize,
    time_start: Option<Instant>,
    duration: Option<Duration>,
}

impl ResultTracker {
    /// Create a new empty tracker
    pub fn new() -> ResultTracker {
        ResultTracker {
            ok: 0,
            error: 0,
            wrong: 0,
            missing: 0,
            time_start: None,
            duration: None,
        }
    }

    /// Create a a new started tracker
    pub fn new_and_start() -> ResultTracker {
        let mut new = Self::new();
        new.start_timer();
        new
    }

    /// Start the timer.
    /// Duration is cleared.
    pub fn start_timer(&mut self) {
        self.duration = None;
        self.time_start = Some(Instant::now())
    }

    /// Stop the timer.
    /// Start time is cleared.
    pub fn stop_timer(&mut self) {
        if let Some(time_start) = self.time_start {
            self.duration = Some(time_start.elapsed());
        }
        self.time_start = None;
    }

    /// Return true if there are no errors
    pub fn has_errors(&self) -> bool {
        self.error > 0
    }

    /// Return a summary for the tracker
    /// ```rust
    /// # use ruri::model::ResultTracker;
    /// let result_tracker = ResultTracker::new();
    /// assert_eq!(result_tracker.summary(), "✓ 0  ⁉ 0  × 0  ‼ 0  ")
    /// ```
    pub fn summary(self) -> String {
        let ok_count = format!("✓ {}", self.ok).green();
        let missing_count = format!("⁉ {}", self.missing).yellow();
        let wrong_count = format!("× {}", self.wrong).red();
        let error_count = format!("‼ {}", self.error).purple();
        let duration = if let Some(duration) = self.duration {
            format!("⏲ {} ", format_duration(duration)).blue()
        } else {
            Default::default()
        };
        format!(
            "{}  {}  {}  {}  {}",
            ok_count, missing_count, wrong_count, error_count, duration
        )
    }

    /// Parse a FileCheckResult and increment the appropriate counters
    pub fn parse_file_check_results<T>(&mut self, results: &BTreeMap<T, FileCheckResult>) {
        for result in results.values() {
            match result {
                Ok(result) => match result.status {
                    Status::Ok => self.ok += 1,
                    Status::Wrong => self.error += 1,
                    Status::Missing => self.missing += 1,
                },
                Err(_) => self.error += 1,
            }
        }
    }
}
