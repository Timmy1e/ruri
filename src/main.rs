/*
 * Copyright © 2022  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::process::ExitCode;

use clap::Parser;

mod check_file;
mod check_files;
mod model;

use crate::{
    check_files::check_files,
    model::{Config, ResultTracker, Status},
};

fn main() -> ExitCode {
    // Parse the arguments into a Config
    let config = Config::parse();
    // If we are running in non-quiet mode, start a new ResultTracker, no need to run it if we are quiet
    let result_tracker = if !config.is_quiet() {
        Some(ResultTracker::new_and_start())
    } else {
        None
    };

    // Check the files in the config, happy flow
    let results = check_files(&config);

    // If we have some result tracker
    if let Some(mut result_tracker) = result_tracker {
        // Stop the timer, so we don't count any of the result parsing
        result_tracker.stop_timer();
        // Parse the results, and count them
        result_tracker.parse_file_check_results(&results);

        // Set the exit code
        let exit_code = if result_tracker.has_errors() {
            // If we have errors, return an error code
            ExitCode::FAILURE
        } else {
            // If we don't have errors, return a success code
            ExitCode::SUCCESS
        };

        // Print the result line
        println!("{}", result_tracker.summary());

        exit_code
    } else {
        let mut exit_code = ExitCode::SUCCESS;
        // If we are quiet, we'll need to loop through the results
        for file_path in config.file_paths() {
            let result = results.get(file_path).unwrap();
            // And print the appropriate text
            match result {
                Ok(result) => {
                    println!("{}  {}", result.crc_calculated, result.file_name);
                    if result.status == Status::Wrong {
                        exit_code = ExitCode::FAILURE;
                    }
                }
                Err(error) => {
                    eprintln!("Could not check {}", error);
                    exit_code = ExitCode::FAILURE;
                }
            }
        }
        exit_code
    }
}
