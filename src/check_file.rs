/*
 * Copyright © 2022  Tim van Leuverden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};

use colored::Colorize;
use crc32fast::Hasher;
use indicatif::ProgressBar;
use lazy_static::lazy_static;
use regex::Regex;

use crate::model::{Error::*, FileCheckAnswer, FileCheckResult, Status};

/// Check a single file, and optionally report progress in a progress bar
pub(crate) fn check_file(
    file_path: &PathBuf,
    progress_bar: Option<ProgressBar>,
) -> FileCheckResult {
    // Get file
    let file = File::open(file_path);
    if file.is_err() {
        if let Some(pb) = &progress_bar {
            pb.println(format!(
                "{}   \t{} could not be opened!",
                "‼  ERROR".purple(),
                file_path.display().to_string().yellow()
            ));
            pb.finish_and_clear();
        }
        return Err(CouldNotOpen(file_path.clone()));
    }
    let file = file.unwrap();

    let file_name = file_path.file_name().unwrap().to_str().unwrap();

    // Fetch file metadata
    let metadata = file.metadata().unwrap();
    // Get the file size from the metadata
    let file_size = metadata.len();
    // Check if the file is, a file...
    if !metadata.is_file() || metadata.is_dir() {
        // If we have some progress bar
        if let Some(pb) = &progress_bar {
            // Print an error message
            pb.println(format!(
                "{}   \t{} is not a file!",
                "‼  ERROR".purple(),
                file_path.display().to_string().yellow()
            ));
            // And finish it
            pb.finish_and_clear();
        }
        // Return a Not A File error
        return Err(NotAFile(file_path.clone()));
    }

    // If we have some progress bar
    if let Some(pb) = &progress_bar {
        // Set the max value, to the file size
        pb.set_length(file_size);
        // Set the prefix to the file name
        pb.set_message(file_name.to_string());
    }

    // Make CRC32 hasher
    let mut hasher = Hasher::new();

    // Calculate the CRC32 sum
    let mut current_position: u64 = 0;
    // Pass the file to a buffer reader
    let mut buffer_reader = BufReader::new(file);
    while current_position != file_size {
        // Get a new buffer with next file data
        let buffer = buffer_reader.fill_buf().unwrap();
        // Get the size of the buffer
        let buffer_size = buffer.len();
        // Pass the new data in the buffer to the hasher
        hasher.update(buffer);
        // Add the buffer size to the current position, to update it
        current_position += buffer_size as u64;
        // Tell the reader to move to the next data
        buffer_reader.consume(buffer_size);

        // If some progress_bar, update the position
        if let Some(pb) = &progress_bar {
            pb.set_position(current_position);
        }
    }
    // Finalize the hasher and calculate the CRC32 sum
    let crc_calculated = format!("{:08X}", hasher.finalize());

    let crc_found = get_crc32_from_file_name(file_name)
        .unwrap_or_default()
        .to_string();

    let status = if crc_found.is_empty() {
        Status::Missing
    } else if crc_calculated.eq_ignore_ascii_case(&crc_found) {
        Status::Ok
    } else {
        Status::Wrong
    };

    let answer = FileCheckAnswer {
        file_name: file_name.to_string(),
        crc_found,
        crc_calculated,
        status,
    };
    if let Some(pb) = &progress_bar {
        pb.println(answer.to_string());
        pb.finish_and_clear();
    }
    Ok(answer)
}

fn get_crc32_from_file_name(file_name: &str) -> Option<&str> {
    lazy_static! {
        static ref REGEX: Regex = Regex::new(r"([a-fA-F0-9]{8})").unwrap();
    }

    match REGEX.find(file_name) {
        Some(found_match) => Some(found_match.as_str()),
        _ => None,
    }
}
